var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');
var toBoolean = require('../sources/toBoolean');

describe('Object sanitizer', function() {

    it('The method toBoolean() must be invoked', function() {
        var input = "name";
        var expected = validator.toBoolean(input);
        var result = toBoolean();
        expect(result).to.equal(expected);
    });

});

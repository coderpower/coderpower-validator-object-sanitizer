var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');
var toInt = require('../sources/toInt');

describe('Object sanitizer', function() {

    it('The method toInt() must be invoked', function() {
        var input = "19";
        var expected = validator.toInt(input);
        var result = toInt();
        expect(result).to.equal(expected);
    });

});

var chai = require('chai');
var expect = chai.expect;
var validator = require('validator');
var toFloat = require('../sources/toFloat');

describe('Object sanitizer', function() {

    it('The method toFloat() must be invoked', function() {
        var input = "14.5";
        var expected = validator.toFloat(input);
        var result = toFloat();
        expect(result).to.equal(expected);
    });

});

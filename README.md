### Object sanitizer

In this example we will cover the following functions:

```javascript
    var inputToBoolean = validator.toBoolean("name");
    var inputToFloat = validator.toFloat("14.5");
    var inputToInt = validator.toInt("19");
```

For more information, please refer to the documentation: https://github.com/chriso/validator.js#readme

var validator = require('validator');

module.exports = function toBoolean() {
    var input = "19";

    var inputToInt = validator.toInt(input);
    console.log('inputToInt: ', inputToInt);

    // The second argument the radix
    var inputToIntWithRadix = validator.toInt(input, 16);
    console.log('inputToIntWithRadix: ', inputToIntWithRadix);

    return inputToInt;
};
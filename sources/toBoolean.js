var validator = require('validator');

module.exports = function toBoolean(){
    var input = "name";

    var inputToBoolean = validator.toBoolean(input);
    console.log('inputToBoolean: ', inputToBoolean);

    // With the second argument we can switch on the strict mode
    var inputToBooleanStrictMode = validator.toBoolean(input, true);
    console.log('inputToBooleanStrictMode: ', inputToBooleanStrictMode);

    return inputToBoolean;
};